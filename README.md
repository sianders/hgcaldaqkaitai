# HGCALDAQKaitai structs
This repository contains the Kaitai structs for the HGCAL DAQ data formats.

Run in container to get access to the kaitai-struct-compiler:

Run `kaitai-struct-compiler --help` to see the available options.

## Usage for python
To compile the kaitai structs to python, run the following command:
```
kaitai-struct-compiler src/emp.ksy -t python --outdir classes
```

Add `--python-package <location>` to get correct imports in the generated python files. 